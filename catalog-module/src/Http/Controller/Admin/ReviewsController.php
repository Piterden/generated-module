<?php namespace Fesor\CatalogModule\Http\Controller\Admin;

use Fesor\CatalogModule\Review\Form\ReviewFormBuilder;
use Fesor\CatalogModule\Review\Table\ReviewTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class ReviewsController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param ReviewTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(ReviewTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param ReviewFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(ReviewFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param ReviewFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(ReviewFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
