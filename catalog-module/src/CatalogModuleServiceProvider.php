<?php namespace Fesor\CatalogModule;

use Anomaly\Streams\Platform\Addon\AddonServiceProvider;
use Fesor\CatalogModule\Review\Contract\ReviewRepositoryInterface;
use Fesor\CatalogModule\Review\ReviewRepository;
use Anomaly\Streams\Platform\Model\Catalog\CatalogReviewsEntryModel;
use Fesor\CatalogModule\Review\ReviewModel;
use Fesor\CatalogModule\Comment\Contract\CommentRepositoryInterface;
use Fesor\CatalogModule\Comment\CommentRepository;
use Anomaly\Streams\Platform\Model\Catalog\CatalogCommentsEntryModel;
use Fesor\CatalogModule\Comment\CommentModel;
use Fesor\CatalogModule\Type\Contract\TypeRepositoryInterface;
use Fesor\CatalogModule\Type\TypeRepository;
use Anomaly\Streams\Platform\Model\Catalog\CatalogTypesEntryModel;
use Fesor\CatalogModule\Type\TypeModel;
use Fesor\CatalogModule\Product\Contract\ProductRepositoryInterface;
use Fesor\CatalogModule\Product\ProductRepository;
use Anomaly\Streams\Platform\Model\Catalog\CatalogProductsEntryModel;
use Fesor\CatalogModule\Product\ProductModel;
use Illuminate\Routing\Router;

class CatalogModuleServiceProvider extends AddonServiceProvider
{

    /**
     * Additional addon plugins.
     *
     * @type array|null
     */
    protected $plugins = [];

    /**
     * The addon Artisan commands.
     *
     * @type array|null
     */
    protected $commands = [];

    /**
     * The addon's scheduled commands.
     *
     * @type array|null
     */
    protected $schedules = [];

    /**
     * The addon API routes.
     *
     * @type array|null
     */
    protected $api = [];

    /**
     * The addon routes.
     *
     * @type array|null
     */
    protected $routes = [
        'admin/catalog/reviews'           => 'Fesor\CatalogModule\Http\Controller\Admin\ReviewsController@index',
        'admin/catalog/reviews/create'    => 'Fesor\CatalogModule\Http\Controller\Admin\ReviewsController@create',
        'admin/catalog/reviews/edit/{id}' => 'Fesor\CatalogModule\Http\Controller\Admin\ReviewsController@edit',
        'admin/catalog/comments'           => 'Fesor\CatalogModule\Http\Controller\Admin\CommentsController@index',
        'admin/catalog/comments/create'    => 'Fesor\CatalogModule\Http\Controller\Admin\CommentsController@create',
        'admin/catalog/comments/edit/{id}' => 'Fesor\CatalogModule\Http\Controller\Admin\CommentsController@edit',
        'admin/catalog/types'           => 'Fesor\CatalogModule\Http\Controller\Admin\TypesController@index',
        'admin/catalog/types/create'    => 'Fesor\CatalogModule\Http\Controller\Admin\TypesController@create',
        'admin/catalog/types/edit/{id}' => 'Fesor\CatalogModule\Http\Controller\Admin\TypesController@edit',
        'admin/catalog/products'           => 'Fesor\CatalogModule\Http\Controller\Admin\ProductsController@index',
        'admin/catalog/products/create'    => 'Fesor\CatalogModule\Http\Controller\Admin\ProductsController@create',
        'admin/catalog/products/edit/{id}' => 'Fesor\CatalogModule\Http\Controller\Admin\ProductsController@edit',
    ];

    /**
     * The addon middleware.
     *
     * @type array|null
     */
    protected $middleware = [
        //Fesor\CatalogModule\Http\Middleware\ExampleMiddleware::class
    ];

    /**
     * The addon route middleware.
     *
     * @type array|null
     */
    protected $routeMiddleware = [];

    /**
     * The addon event listeners.
     *
     * @type array|null
     */
    protected $listeners = [
        //Fesor\CatalogModule\Event\ExampleEvent::class => [
        //    Fesor\CatalogModule\Listener\ExampleListener::class,
        //],
    ];

    /**
     * The addon alias bindings.
     *
     * @type array|null
     */
    protected $aliases = [
        //'Example' => Fesor\CatalogModule\Example::class
    ];

    /**
     * The addon class bindings.
     *
     * @type array|null
     */
    protected $bindings = [
        CatalogReviewsEntryModel::class => ReviewModel::class,
        CatalogCommentsEntryModel::class => CommentModel::class,
        CatalogTypesEntryModel::class => TypeModel::class,
        CatalogProductsEntryModel::class => ProductModel::class,
    ];

    /**
     * The addon singleton bindings.
     *
     * @type array|null
     */
    protected $singletons = [
        ReviewRepositoryInterface::class => ReviewRepository::class,
        CommentRepositoryInterface::class => CommentRepository::class,
        TypeRepositoryInterface::class => TypeRepository::class,
        ProductRepositoryInterface::class => ProductRepository::class,
    ];

    /**
     * Additional service providers.
     *
     * @type array|null
     */
    protected $providers = [
        //\ExamplePackage\Provider\ExampleProvider::class
    ];

    /**
     * The addon view overrides.
     *
     * @type array|null
     */
    protected $overrides = [
        //'streams::errors/404' => 'module::errors/404',
        //'streams::errors/500' => 'module::errors/500',
    ];

    /**
     * The addon mobile-only view overrides.
     *
     * @type array|null
     */
    protected $mobile = [
        //'streams::errors/404' => 'module::mobile/errors/404',
        //'streams::errors/500' => 'module::mobile/errors/500',
    ];

    /**
     * Register the addon.
     */
    public function register()
    {
        // Run extra pre-boot registration logic here.
        // Use method injection or commands to bring in services.
    }

    /**
     * Boot the addon.
     */
    public function boot()
    {
        // Run extra post-boot registration logic here.
        // Use method injection or commands to bring in services.
    }

    /**
     * Map additional addon routes.
     *
     * @param Router $router
     */
    public function map(Router $router)
    {
        // Register dynamic routes here for example.
        // Use method injection or commands to bring in services.
    }

}
