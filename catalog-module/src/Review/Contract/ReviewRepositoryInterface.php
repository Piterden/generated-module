<?php namespace Fesor\CatalogModule\Review\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface ReviewRepositoryInterface extends EntryRepositoryInterface
{

}
