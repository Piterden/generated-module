<?php namespace Fesor\CatalogModule\Review;

use Fesor\CatalogModule\Review\Contract\ReviewRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class ReviewRepository extends EntryRepository implements ReviewRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var ReviewModel
     */
    protected $model;

    /**
     * Create a new ReviewRepository instance.
     *
     * @param ReviewModel $model
     */
    public function __construct(ReviewModel $model)
    {
        $this->model = $model;
    }
}
