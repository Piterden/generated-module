<?php namespace Fesor\CatalogModule\Product\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface ProductRepositoryInterface extends EntryRepositoryInterface
{

}
